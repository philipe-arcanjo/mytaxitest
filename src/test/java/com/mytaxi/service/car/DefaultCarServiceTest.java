package com.mytaxi.service.car;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.ManufacturerRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.BusinessLogicException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.exception.NoCarFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DefaultCarServiceTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private DefaultCarService defaultCarService;

    @Mock
    private CarRepository carRepository;

    @Mock
    private ManufacturerRepository manufacturerRepository;

    private CarDO carDO;

    @Before
    public void setUp()
    {
        ManufacturerDO manufacturer = new ManufacturerDO("Audi");
        manufacturer.setId(1L);
        carDO = new CarDO("OOO", 5, false, 4, "electric", manufacturer);
    }

    @Test
    public void shouldFindACar() throws EntityNotFoundException
    {
        // given
        Long id = 1L;

        carDO.setId(id);

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(carDO));

        CarDO carDO = defaultCarService.find(id);

        // then
        verify(carRepository).findByIdAndDeletedFalse(id);

        assertNotNull(carDO);
        assertEquals(carDO.getId(), carDO.getId());
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarIsFound() throws EntityNotFoundException
    {
        // given
        Long id = 1L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultCarService.find(id);
    }

    @Test
    public void shouldFindAll() throws NoCarFoundException
    {
        // given
        CarDO car1 = new CarDO( "OOO", 5, false, 3, "electric", new ManufacturerDO("Audi"));
        CarDO car2 = new CarDO("EEE", 5, false, 3, "electric", new ManufacturerDO("Audi"));

        // when
        when(carRepository.findAllByDeletedFalse()).thenReturn(asList(car1, car2));

        List<CarDO> cars = defaultCarService.findAll();

        // then
        verify(carRepository).findAllByDeletedFalse();

        assertNotNull(cars);
        assertThat(cars, hasSize(2));
        assertThat(cars.get(0).getLicensePlate(), is("OOO"));
        assertThat(cars.get(1).getLicensePlate(), is("EEE"));
    }

    @Test
    public void shouldThrowNoCarFoundExceptionWhenNoCarIsFound() throws NoCarFoundException
    {
        // given
        expectedException.expect(NoCarFoundException.class);
        expectedException.expectMessage("No car was found!");

        // when
        when(carRepository.findAllByDeletedFalse()).thenReturn(emptyList());

        defaultCarService.findAll();
    }

    @Test
    public void shouldCreateACar() throws ConstraintsViolationException, BusinessLogicException
    {
        // given
        CarDO savedCar = carDO;
        savedCar.setId(1L);

        ManufacturerDO manufacturer = carDO.getManufacturer();

        // when
        when(manufacturerRepository.findById(manufacturer.getId())).thenReturn(Optional.of(manufacturer));
        when(carRepository.save(carDO)).thenReturn(savedCar);

        CarDO carDOResponse = defaultCarService.create(carDO);

        // then
        verify(manufacturerRepository).findById(manufacturer.getId());
        verify(carRepository).save(carDO);

        assertNotNull(carDOResponse);
        assertNotNull(carDOResponse.getManufacturer());
        assertThat(carDOResponse.getId(), is(savedCar.getId()));
        assertThat(carDOResponse.getLicensePlate(), is("OOO"));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenCreatingCarWithInvalidManufacturer() throws ConstraintsViolationException, BusinessLogicException
    {
        // given
        expectedException.expect(BusinessLogicException.class);
        expectedException.expectMessage("Invalid manufacture.");

        ManufacturerDO manufacturer = carDO.getManufacturer();

        // when
        when(manufacturerRepository.findById(manufacturer.getId())).thenReturn(Optional.empty());

        defaultCarService.create(carDO);

        // then
        verifyZeroInteractions(carRepository.save(carDO));
    }

    @Test
    public void shouldDeleteACar() throws EntityNotFoundException
    {
        // given
        Long id = 1L;
        carDO.setId(id);

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(carDO));

        defaultCarService.delete(id);

        // then
        verify(carRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarToBeDeletedIsFound() throws EntityNotFoundException
    {
        // given
        Long id = 5L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultCarService.delete(id);
    }

    @Test
    public void shouldUpdateACar() throws ConstraintsViolationException, EntityNotFoundException, BusinessLogicException
    {
        // given
        Long id = 5L;

        ManufacturerDO manufacturerFromDB = new ManufacturerDO("BMW");
        manufacturerFromDB.setId(2L);

        CarDO carFromDB = new CarDO("EEE", 5, false, 4, "Gas", manufacturerFromDB);

        ManufacturerDO manufacturer = carDO.getManufacturer();

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(carFromDB));
        when(manufacturerRepository.findById(manufacturer.getId())).thenReturn(Optional.of(manufacturer));
        when(carRepository.save(carFromDB)).thenReturn(carFromDB);

        CarDO carDOResponse = defaultCarService.update(carDO, id);

        // then
        verify(manufacturerRepository).findById(manufacturer.getId());
        verify(carRepository).findByIdAndDeletedFalse(id);
        verify(carRepository).save(carFromDB);

        assertNotNull(carDOResponse);
        assertNotNull(carDOResponse.getManufacturer());
        assertThat(carDOResponse.getManufacturer().getId(), is(carDO.getManufacturer().getId()));
        assertThat(carDOResponse.getManufacturer().getName(), is(carDO.getManufacturer().getName()));
        assertThat(carDOResponse.getLicensePlate(), is(carDO.getLicensePlate()));
        assertThat(carDOResponse.getSeatCount(), is(carDO.getSeatCount()));
        assertThat(carDOResponse.getConvertible(), is(carDO.getConvertible()));
        assertThat(carDOResponse.getRating(), is(carDO.getRating()));
        assertThat(carDOResponse.getEngineType(), is(carDO.getEngineType()));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenUpdatingCarWithInvalidManufacturer() throws ConstraintsViolationException, EntityNotFoundException, BusinessLogicException
    {
        // given
        expectedException.expect(BusinessLogicException.class);
        expectedException.expectMessage("Invalid manufacture.");

        Long id = 5L;

        ManufacturerDO manufacturer = carDO.getManufacturer();

        ManufacturerDO manufacturerFromDB = new ManufacturerDO("BMW");
        manufacturerFromDB.setId(2L);

        CarDO carFromDB = new CarDO("EEE", 5, false, 4, "Gas", manufacturerFromDB);

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(carFromDB));
        when(manufacturerRepository.findById(manufacturer.getId())).thenReturn(Optional.empty());

        defaultCarService.update(carDO, id);

        // then
        verifyZeroInteractions(carRepository.save(carDO));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarToBeUpdatedIsFound() throws ConstraintsViolationException, EntityNotFoundException, BusinessLogicException
    {
        // given
        Long id = 6L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(carRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultCarService.update(carDO, id);

        // then
        verifyZeroInteractions(carRepository.save(carDO));
    }
}
