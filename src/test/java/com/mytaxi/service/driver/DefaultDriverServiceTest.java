package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.BusinessLogicException;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DefaultDriverServiceTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private DefaultDriverService defaultDriverService;

    @Mock
    private DriverRepository driverRepository;

    @Mock
    private CarRepository carRepository;

    private DriverDO driverDO;

    private CarDO carDO;

    private CarDO selectCarDO;

    @Before
    public void setUp()
    {
        driverDO = new DriverDO("user1", "pass1");
        driverDO.setOnlineStatus(OnlineStatus.ONLINE);

        carDO = new CarDO("OOO-1111", 5, false, 4, "Gas", new ManufacturerDO("BMW"));

        selectCarDO = new CarDO("OOO-1111");
    }

    @Test
    public void shouldFindADriver() throws EntityNotFoundException
    {
        // given
        Long id = 1L;
        driverDO.setId(id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(driverDO));

        DriverDO driverDOResponse = defaultDriverService.find(id);

        // then
        verify(driverRepository).findByIdAndDeletedFalse(id);

        assertNotNull(driverDOResponse);
        assertThat(driverDOResponse.getId(), is(id));
        assertThat(driverDOResponse.getUsername(), is("user1"));
        assertThat(driverDOResponse.getPassword(), is("pass1"));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverIsFound() throws EntityNotFoundException
    {
        // given
        Long id = 2L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultDriverService.find(id);
    }

    @Test
    public void shouldCreateADriver() throws ConstraintsViolationException
    {
        // given
        DriverDO savedDriver = driverDO;
        savedDriver.setId(9L);

        // when
        when(driverRepository.save(driverDO)).thenReturn(savedDriver);

        DriverDO driverDOResponse = defaultDriverService.create(driverDO);

        // then
        verify(driverRepository).save(driverDO);

        assertNotNull(driverDOResponse);
        assertThat(savedDriver.getId(), is(9L));
        assertThat(savedDriver.getUsername(), is("user1"));
        assertThat(savedDriver.getPassword(), is("pass1"));
    }

    @Test
    public void shouldDeleteADriver() throws EntityNotFoundException
    {
        // given
        Long id = 3L;
        driverDO.setId(id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(driverDO));

        defaultDriverService.delete(id);

        // then
        verify(driverRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverToBeDeletedIsFound() throws EntityNotFoundException
    {
        // given
        Long id = 4L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultDriverService.delete(id);
    }

    @Test
    public void shouldUpdateADriverLocation() throws EntityNotFoundException
    {
        // given
        Long id = 3L;
        driverDO.setId(id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.of(driverDO));

        defaultDriverService.updateLocation(id, 1.1, 2.2);

        // then
        verify(driverRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverToBeUpdatedIsFound() throws EntityNotFoundException
    {
        // given
        Long id = 5L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultDriverService.updateLocation(id, 1.1, 2.2);
    }

    @Test
    public void shouldFindDriversByOnlineStatus()
    {
        // given
        OnlineStatus status = OnlineStatus.ONLINE;

        DriverDO driver1 = new DriverDO();
        driver1.setOnlineStatus(status);

        DriverDO driver2 = new DriverDO();
        driver2.setOnlineStatus(status);

        // when
        when(driverRepository.findAll(any(Example.class))).thenReturn(asList(driver1, driver2));

        List<DriverDO> driversResponse = defaultDriverService.findByAttributes(status, null, null, null, null, null);

        // then
        verify(driverRepository).findAll(any(Example.class));

        assertNotNull(driversResponse);
        assertThat(driversResponse, hasSize(2));
        assertThat(driversResponse.get(0).getOnlineStatus(), is(driver1.getOnlineStatus()));
        assertThat(driversResponse.get(1).getOnlineStatus(), is(driver1.getOnlineStatus()));
    }

    @Test
    public void shouldFindDriverByUserName()
    {
        // given
        DriverDO driver = new DriverDO();
        driver.setUsername("driver01");

        // when
        when(driverRepository.findAll(any(Example.class))).thenReturn(singletonList(driver));

        List<DriverDO> driversResponse = defaultDriverService.findByAttributes(null, "driver01", null, null, null, null);

        // then
        verify(driverRepository).findAll(any(Example.class));

        assertNotNull(driversResponse);
        assertThat(driversResponse, hasSize(1));
        assertThat(driversResponse.get(0).getUsername(), is(driver.getUsername()));
    }

    @Test
    public void shouldFindDriverByDriverAndCarAttributes()
    {
        // given
        CarDO car = new CarDO();
        car.setLicensePlate("AAA-1111");
        car.setSeatCount(5);

        DriverDO driver = new DriverDO();
        driver.setUsername("driver01");
        driver.setCar(car);

        // when
        when(driverRepository.findAll(any(Example.class))).thenReturn(singletonList(driver));

        List<DriverDO> driversResponse = defaultDriverService.findByAttributes(null, "driver01", "AAA-1111", null, 5, null);

        // then
        verify(driverRepository).findAll(any(Example.class));

        assertNotNull(driversResponse);
        assertThat(driversResponse, hasSize(1));
        assertThat(driversResponse.get(0).getUsername(), is(driver.getUsername()));
        assertThat(driversResponse.get(0).getCar().getLicensePlate(), is(driver.getCar().getLicensePlate()));
        assertThat(driversResponse.get(0).getCar().getSeatCount(), is(driver.getCar().getSeatCount()));
    }

    @Test
    public void shouldNotFindDriversWhenSearchingForCarAttributesOfAnUnselectedCar()
    {
        CarDO carExample = new CarDO();
        carExample.setLicensePlate("AAA-1111");

        DriverDO driverExample = new DriverDO();
        driverExample.setCar(carExample);

        ExampleMatcher matcher = ExampleMatcher.matching()
            .withIgnorePaths("dateCreated", "dateCoordinateUpdated", "car.busy", "car.dateCreated");

        Example<DriverDO> example = Example.of(driverExample, matcher);

        // when
        when(driverRepository.findAll(example)).thenReturn(emptyList());

        List<DriverDO> driversResponse = defaultDriverService.findByAttributes(null, null, "AAA-1111", null, null, null);

        // then
        verify(driverRepository).findAll(any(Example.class));

        assertNotNull(driversResponse);
        assertThat(driversResponse, hasSize(0));
    }

    @Test
    public void shouldSelectACar() throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        // given
        Long driverId = 1L;
        driverDO.setId(driverId);

        Long carId = 1L;
        carDO.setId(carId);

        String licensePlate = selectCarDO.getLicensePlate();

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));
        when(carRepository.findByLicensePlateAndDeletedFalse(licensePlate)).thenReturn(Optional.of(carDO));
        when(driverRepository.save(driverDO)).thenReturn(driverDO);

        DriverDO driverDOResponse = defaultDriverService.selectCar(driverId, selectCarDO);

        // then
        verify(driverRepository).findByIdAndDeletedFalse(driverId);
        verify(carRepository).findByLicensePlateAndDeletedFalse(licensePlate);
        verify(driverRepository).save(driverDO);

        assertNotNull(driverDOResponse);
        assertNotNull(driverDOResponse.getCar());
        assertThat(driverDOResponse.getCar().getLicensePlate(), is(licensePlate));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenAnOfflineDriverSelectsACar() throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        // given
        expectedException.expect(BusinessLogicException.class);
        expectedException.expectMessage("A car can be selected only by ONLINE drivers.");

        Long driverId = 1L;
        driverDO.setId(driverId);
        driverDO.setOnlineStatus(OnlineStatus.OFFLINE);

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));

        defaultDriverService.selectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(carRepository.findByLicensePlateAndDeletedFalse(selectCarDO.getLicensePlate()));
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenADriverWithASelectedCarSelectsACar() throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        // given
        expectedException.expect(BusinessLogicException.class);
        expectedException.expectMessage("The driver already has a selected car.");

        Long driverId = 1L;
        driverDO.setId(driverId);
        driverDO.setCar(new CarDO());

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));

        defaultDriverService.selectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(carRepository.findByLicensePlateAndDeletedFalse(selectCarDO.getLicensePlate()));
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowCarAlreadyInUseExceptionWhenADriverSelectsABusyCar() throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        // given
        expectedException.expect(CarAlreadyInUseException.class);
        expectedException.expectMessage("The car is already in use.");

        Long driverId = 1L;
        driverDO.setId(driverId);

        Long carId = 1L;
        carDO.setId(carId);
        carDO.setBusy(true);

        String licensePlate = selectCarDO.getLicensePlate();

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));
        when(carRepository.findByLicensePlateAndDeletedFalse(licensePlate)).thenReturn(Optional.of(carDO));

        defaultDriverService.selectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverIsFoundToSelectACar() throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        // given
        Long id = 5L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultDriverService.selectCar(id, selectCarDO);

        // then
        verifyZeroInteractions(carRepository.findByLicensePlateAndDeletedFalse(selectCarDO.getLicensePlate()));
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarIsFoundToSelectACar() throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        // given
        Long driverId = 1L;
        driverDO.setId(driverId);

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find car with license plate: " + selectCarDO.getLicensePlate());

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));
        when(carRepository.findByLicensePlateAndDeletedFalse(selectCarDO.getLicensePlate())).thenReturn(Optional.empty());

        defaultDriverService.selectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldDeselectCar() throws EntityNotFoundException, BusinessLogicException
    {
        // given
        Long driverId = 1L;
        driverDO.setId(driverId);
        driverDO.setCar(carDO);

        String licensePlate = selectCarDO.getLicensePlate();

        Long carId = 1L;
        carDO.setId(carId);
        carDO.setBusy(true);

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));
        when(carRepository.findByLicensePlateAndDeletedFalse(licensePlate)).thenReturn(Optional.of(carDO));
        when(driverRepository.save(driverDO)).thenReturn(driverDO);

        DriverDO driverDOResponse = defaultDriverService.deselectCar(driverId, selectCarDO);

        // then
        verify(driverRepository).findByIdAndDeletedFalse(driverId);

        assertNotNull(driverDOResponse);
        assertNull(driverDOResponse.getCar());
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverIsFoundToDeselectACar() throws EntityNotFoundException, BusinessLogicException
    {
        // given
        Long id = 5L;

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find entity with id: " + id);

        // when
        when(driverRepository.findByIdAndDeletedFalse(id)).thenReturn(Optional.empty());

        defaultDriverService.deselectCar(id, selectCarDO);

        // then
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenDriverDoesNotHaveACarForDeselect() throws BusinessLogicException, EntityNotFoundException
    {
        expectedException.expect(BusinessLogicException.class);
        expectedException.expectMessage("The driver does not have any car selected.");

        // given
        Long driverId = 1L;
        driverDO.setId(driverId);

        String licensePlate = selectCarDO.getLicensePlate();

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));

        defaultDriverService.deselectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(carRepository.findByLicensePlateAndDeletedFalse(licensePlate));
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenDriversCarDoesNotMatchWithGivenLicensePlate() throws BusinessLogicException, EntityNotFoundException
    {
        // given
        expectedException.expect(BusinessLogicException.class);
        expectedException.expectMessage("The driver does not have a selected car with the given license plate: ");

        Long driverId = 1L;
        driverDO.setId(driverId);
        carDO.setLicensePlate("ZZZ-1111");
        driverDO.setCar(carDO);

        String licensePlate = selectCarDO.getLicensePlate();

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));

        defaultDriverService.deselectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(carRepository.findByLicensePlateAndDeletedFalse(licensePlate));
        verifyZeroInteractions(driverRepository.save(driverDO));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarIsFoundToDeselectACar() throws BusinessLogicException, EntityNotFoundException
    {
        // given
        Long driverId = 1L;
        driverDO.setId(driverId);
        driverDO.setCar(carDO);

        expectedException.expect(EntityNotFoundException.class);
        expectedException.expectMessage("Could not find car with license plate: " + selectCarDO.getLicensePlate());

        // when
        when(driverRepository.findByIdAndDeletedFalse(driverId)).thenReturn(Optional.of(driverDO));
        when(carRepository.findByLicensePlateAndDeletedFalse(selectCarDO.getLicensePlate())).thenReturn(Optional.empty());

        defaultDriverService.deselectCar(driverId, selectCarDO);

        // then
        verifyZeroInteractions(driverRepository.save(driverDO));
    }
}
