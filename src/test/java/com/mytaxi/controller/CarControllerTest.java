package com.mytaxi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.MytaxiServerApplicantTestApplication;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.ManufacturerDO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MytaxiServerApplicantTestApplication.class)
@AutoConfigureMockMvc
@Transactional
public class CarControllerTest
{

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static final String BASE_URL = "/v1/cars";

    private CarDTO.CarDTOBuilder carDTOBuilder;

    @Before
    public void setUp()
    {
        carDTOBuilder = CarDTO.newBuilder()
            .setLicensePlate("AAF-1111")
            .setSeatCount(5)
            .setConvertible(false)
            .setRating(4)
            .setEngineType("Gas")
            .setManufacturer(new ManufacturerDO("Audi"));
    }

    @Test
    public void shouldFindACar() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.licensePlate", is("AAA-1111")))
            .andExpect(jsonPath("$.seatCount", is(5)))
            .andExpect(jsonPath("$.convertible", is(true)))
            .andExpect(jsonPath("$.rating", is(5)))
            .andExpect(jsonPath("$.engineType", is("electric")))
            .andExpect(jsonPath("$.manufacturer.id", is(3)))
            .andExpect(jsonPath("$.manufacturer.name", is("Mercedes")));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarIsFound() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "/999")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFindAllCars() throws Exception
    {
        mockMvc.perform(get(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(5)))
            .andExpect(jsonPath("$[*].id", contains(1, 2, 3, 4, 5)));
    }

    @Test
    public void shouldThrowNoCarFoundExceptionWhenNoCarIsFound() throws Exception
    {
        mockMvc.perform(delete(BASE_URL + "/1"));
        mockMvc.perform(delete(BASE_URL + "/2"));
        mockMvc.perform(delete(BASE_URL + "/3"));
        mockMvc.perform(delete(BASE_URL + "/4"));
        mockMvc.perform(delete(BASE_URL + "/5"));

        mockMvc.perform(get(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent())
            .andExpect(content().string(""));
    }

    @Test
    public void shouldDeleteACar() throws Exception
    {
        mockMvc.perform(delete(BASE_URL + "/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent())
            .andExpect(content().string(""));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarToBeDeletedIsFound() throws Exception
    {
        mockMvc.perform(delete(BASE_URL + "/999")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldCreateACar() throws Exception
    {
        mockMvc.perform(post(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.licensePlate", is("AAF-1111")))
            .andExpect(jsonPath("$.seatCount", is(5)))
            .andExpect(jsonPath("$.convertible", is(false)))
            .andExpect(jsonPath("$.rating", is(4)))
            .andExpect(jsonPath("$.engineType", is("Gas")))
            .andExpect(jsonPath("$.manufacturer.name", is("Audi")));
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenCreatingCarWithInvalidManufacturer() throws Exception
    {
        ManufacturerDO manufacturerDO = new ManufacturerDO("InvalidManufacturer");
        manufacturerDO.setId(999L);
        carDTOBuilder.setManufacturer(manufacturerDO);

        mockMvc.perform(post(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowConstraintsViolationExceptionWhenCreatingCarWithLicensePlateAlreadyUsed() throws Exception
    {
        carDTOBuilder.setLicensePlate("AAA-1111");

        mockMvc.perform(post(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldOnlyAllowPostHttpMethodWhenCreatingACar() throws Exception
    {
        mockMvc.perform(put(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void shouldUpdateACar() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.licensePlate", is("AAF-1111")))
            .andExpect(jsonPath("$.seatCount", is(5)))
            .andExpect(jsonPath("$.convertible", is(false)))
            .andExpect(jsonPath("$.rating", is(4)))
            .andExpect(jsonPath("$.engineType", is("Gas")))
            .andExpect(jsonPath("$.manufacturer.name", is("Audi")));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarToBeUpdatedIsFound() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/999")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenUpdatingCarWithInvalidManufacturer() throws Exception
    {
        ManufacturerDO manufacturerDO = new ManufacturerDO("InvalidManufacturer");
        manufacturerDO.setId(999L);
        carDTOBuilder.setManufacturer(manufacturerDO);

        mockMvc.perform(put(BASE_URL + "/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }
}
