package com.mytaxi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.MytaxiServerApplicantTestApplication;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainvalue.GeoCoordinate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MytaxiServerApplicantTestApplication.class)
@AutoConfigureMockMvc
@Transactional
public class DriverControllerTest
{

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static final String BASE_URL = "/v1/drivers";

    private DriverDTO.DriverDTOBuilder driverDTOBuilder;

    private CarDTO.CarDTOBuilder carDTOBuilder;


    @Before
    public void setUp()
    {
        driverDTOBuilder = DriverDTO.newBuilder()
            .setPassword("pass1")
            .setUsername("user1")
            .setCoordinate(new GeoCoordinate(1.1, 1.2));

        carDTOBuilder = CarDTO.newBuilder()
            .setLicensePlate("AAE-1111");
    }

    @Test
    public void shouldCreateADriver() throws Exception
    {
        mockMvc.perform(post(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(driverDTOBuilder.createDriverDTO())))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.username", is("user1")))
            .andExpect(jsonPath("$.password", is("pass1")));
    }

    @Test
    public void shouldThrowConstraintsViolationExceptionWhenUsernameAlreadyExists() throws Exception
    {
        driverDTOBuilder.setUsername("driver01");

        mockMvc.perform(post(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(driverDTOBuilder.createDriverDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldOnlyAllowPostHttpMethodWhenCreatingADriver() throws Exception
    {
        mockMvc.perform(put(BASE_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(driverDTOBuilder.createDriverDTO())))
            .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void shouldFindADriver() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.username", is("driver01")))
            .andExpect(jsonPath("$.password", is("driver01pw")));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverIsFound() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "/999")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldUpdateADriverLocation() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/8?latitude=8.5&longitude=56.954")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string(""));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverToBeUpdatedIsFound() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/999?latitude=8.5&longitude=56.954")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldDeleteADriver() throws Exception
    {
        mockMvc.perform(delete(BASE_URL + "/1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent())
            .andExpect(content().string(""));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverToBeDeletedIsFound() throws Exception
    {
        mockMvc.perform(delete(BASE_URL + "/999")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFindDriversByOnlineStatus() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "?onlineStatus=ONLINE")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(4)))
            .andExpect(jsonPath("$[*].id", contains(4, 5, 6, 8)));
    }

    @Test
    public void shouldFindDriverByOnlineStatusAndUserName() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "?onlineStatus=ONLINE&userName=driver04")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].username", is("driver04")))
            .andExpect(jsonPath("$.[0].password", is("driver04pw")));
    }

    @Test
    public void shouldNotFindDriversWhenFindByDriverAndCarAttributesOfAnUnselectedCar() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "?onlineStatus=ONLINE&userName=driver04&licensePlate=AAA-1111")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void shouldNotFindDriversWhenFindOnlyByCarAttributesOfAnUnselectedCar() throws Exception
    {
        mockMvc.perform(get(BASE_URL + "?licensePlate=AAA-1111&rating=5")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void shouldFindDriverByOnlineStatusAndUserNameAndLicensePlate() throws Exception
    {
        // Selecting the car AAE-1111
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())));

        // Searching for a driver with the given attributes
        mockMvc.perform(get(BASE_URL + "?onlineStatus=ONLINE&userName=driver04&licensePlate=AAE-1111")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].username", is("driver04")))
            .andExpect(jsonPath("$.[0].password", is("driver04pw")))
            .andExpect(jsonPath("$.[0].selectedCar", is("AAE-1111")));
    }

    @Test
    public void shouldFindDriverByAllPossibleAttributesTogether() throws Exception
    {
        // Selecting the car AAE-1111
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())));

        // Searching for a driver with the given attributes
        mockMvc.perform(get(BASE_URL + "?onlineStatus=ONLINE&userName=driver04&licensePlate=AAE-1111&rating=2&seatCount=2&convertible=false")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].username", is("driver04")))
            .andExpect(jsonPath("$.[0].password", is("driver04pw")))
            .andExpect(jsonPath("$.[0].selectedCar", is("AAE-1111")));
    }

    @Test
    public void shouldNotFindDriversWhenCarAttributesDoNotMatchTheSelectedCar() throws Exception
    {
        // Selecting the car AAE-1111
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())));

        // Searching for a driver with the given attributes
        mockMvc.perform(get(BASE_URL + "?onlineStatus=ONLINE&userName=driver04&licensePlate=AAE-1111&rating=3&seatCount=2&convertible=false")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void shouldSelectACar() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.username", is("driver04")))
            .andExpect(jsonPath("$.password", is("driver04pw")))
            .andExpect(jsonPath("$.selectedCar", is("AAE-1111")));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverIsFoundToSelectACar() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/selectCar/999")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenAnOfflineDriverSelectsACar() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/selectCar/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenADriverWithASelectedCarSelectsACar() throws Exception
    {
        // Selects a car for driver 4
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())));

        // Selects a car again with same driver that already has a car selected
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowCarAlreadyInUseExceptionWhenADriverSelectsABusyCar() throws Exception
    {
        // A driver selects the car AAE-1111
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())));

        // Another driver selects the car selected previously
        mockMvc.perform(put(BASE_URL + "/selectCar/5")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoCarIsFoundToSelectACar() throws Exception
    {
        carDTOBuilder = CarDTO.newBuilder()
            .setLicensePlate("AAF-1111");

        mockMvc.perform(put(BASE_URL + "/selectCar/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldDeselectCar() throws Exception
    {
        // Selecting a car
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.username", is("driver04")))
            .andExpect(jsonPath("$.password", is("driver04pw")))
            .andExpect(jsonPath("$.selectedCar", is("AAE-1111")));

        // Deselecting a car
        mockMvc.perform(put(BASE_URL + "/deselectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.username", is("driver04")))
            .andExpect(jsonPath("$.password", is("driver04pw")))
            .andExpect(jsonPath("$.selectedCar").doesNotExist());
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenNoDriverIsFoundToDeselectACar() throws Exception
    {
        // Selecting a car
        mockMvc.perform(put(BASE_URL + "/deselectCar/999")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenDriverDoesNotHaveACarForDeselect() throws Exception
    {
        mockMvc.perform(put(BASE_URL + "/deselectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldThrowBusinessLogicExceptionWhenDriversCarDoesNotMatchWithGivenLicensePlate() throws Exception
    {
        // Selecting a car
        mockMvc.perform(put(BASE_URL + "/selectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.username", is("driver04")))
            .andExpect(jsonPath("$.password", is("driver04pw")))
            .andExpect(jsonPath("$.selectedCar", is("AAE-1111")));

        carDTOBuilder = CarDTO.newBuilder()
            .setLicensePlate("AAF-1111");

        // Deselecting car for the same driver, but another license plate
        mockMvc.perform(put(BASE_URL + "/deselectCar/4")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(carDTOBuilder.createCarDTO())))
            .andExpect(status().isBadRequest());
    }
}
