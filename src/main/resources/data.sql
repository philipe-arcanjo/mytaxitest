/**
 * CREATE Script for init of DB
 */

------------------------------------------------------ DRIVERS ------------------------------------------------------

-- Create 3 OFFLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username)
values (1, now(), false, 'OFFLINE', 'driver01pw', 'driver01');

insert into driver (id, date_created, deleted, online_status, password, username)
values (2, now(), false, 'OFFLINE', 'driver02pw', 'driver02');

insert into driver (id, date_created, deleted, online_status, password, username)
values (3, now(), false, 'OFFLINE', 'driver03pw', 'driver03');

-- Create 3 ONLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username)
values (4, now(), false, 'ONLINE', 'driver04pw', 'driver04');

insert into driver (id, date_created, deleted, online_status, password, username)
values (5, now(), false, 'ONLINE', 'driver05pw', 'driver05');

insert into driver (id, date_created, deleted, online_status, password, username)
values (6, now(), false, 'ONLINE', 'driver06pw', 'driver06');

-- Create 1 OFFLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (7,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'OFFLINE',
'driver07pw', 'driver07');

-- Create 1 ONLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (8,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'ONLINE',
'driver08pw', 'driver08');

--------------------------------------------------- MANUFACTURERS ----------------------------------------------------

insert into manufacturer (id, date_created, name) values (1, now(), 'Audi');

insert into manufacturer (id, date_created, name) values (2, now(), 'BMW');

insert into manufacturer (id, date_created, name) values (3, now(), 'Mercedes');

-------------------------------------------------------- CARS -------------------------------------------------------

insert into car (id, date_created, deleted, license_plate, seat_count, convertible, rating, engine_type, manufacturer_id, busy)
values (1, now(), false, 'AAA-1111', 5, true, 5, 'electric', 3, false);

insert into car (id, date_created, deleted, license_plate, seat_count, convertible, rating, engine_type, manufacturer_id, busy)
values (2, now(), false, 'AAB-1111', 7, false, 5, 'gas', 2, false);

insert into car (id, date_created, deleted, license_plate, seat_count, convertible, rating, engine_type, manufacturer_id, busy)
values (3, now(), false, 'AAC-1111', 5, true, 4, 'electric', 1, false);

insert into car (id, date_created, deleted, license_plate, seat_count, convertible, rating, engine_type, manufacturer_id, busy)
values (4, now(), false, 'AAD-1111', 5, false, 3, 'gas', 2, false);

insert into car (id, date_created, deleted, license_plate, seat_count, convertible, rating, engine_type, manufacturer_id, busy)
values (5, now(), false, 'AAE-1111', 2, false, 2, 'gas', 1, false);
