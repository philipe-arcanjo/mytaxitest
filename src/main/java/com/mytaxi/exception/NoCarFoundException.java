package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class NoCarFoundException extends Exception
{

    private static final long serialVersionUID = -1334576929313395343L;

    public NoCarFoundException(String message)
    {
        super(message);
    }
}
