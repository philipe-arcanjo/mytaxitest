package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.SelectCarDTO;
import com.mytaxi.domainobject.CarDO;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class CarMapper
{

    public static CarDTO makeCarDTO(CarDO carDO)
    {
        CarDTO.CarDTOBuilder carDTOBuilder = CarDTO.newBuilder()
            .setId(carDO.getId())
            .setConvertible(carDO.getConvertible())
            .setEngineType(carDO.getEngineType())
            .setLicensePlate(carDO.getLicensePlate())
            .setManufacturer(carDO.getManufacturer())
            .setRating(carDO.getRating())
            .setSeatCount(carDO.getSeatCount());

        return carDTOBuilder.createCarDTO();
    }

    public static List<CarDTO> makeCarDTOList(Collection<CarDO> cars)
    {
        return cars.stream()
            .map(CarMapper::makeCarDTO)
            .collect(toList());
    }

    public static CarDO makeCarDO(CarDTO carDTO)
    {
        return new CarDO(carDTO.getLicensePlate(),
            carDTO.getSeatCount(), carDTO.getConvertible(),
            carDTO.getRating(), carDTO.getEngineType(), carDTO.getManufacturer());
    }

    public static CarDO makeCarDO(SelectCarDTO selectCarDTO)
    {
        return new CarDO(selectCarDTO.getLicensePlate());
    }
}
