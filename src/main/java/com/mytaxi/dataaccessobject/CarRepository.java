package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.NoCarFoundException;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Database Access Object for car table.
 * <p/>
 */
public interface CarRepository extends CrudRepository<CarDO, Long>
{

    List<CarDO> findAllByDeletedFalse() throws NoCarFoundException;

    Optional<CarDO> findByIdAndDeletedFalse(Long id);

    Optional<CarDO> findByLicensePlateAndDeletedFalse(String licensePlate);
}
