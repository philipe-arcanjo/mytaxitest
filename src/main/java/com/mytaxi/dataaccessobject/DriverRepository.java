package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.DriverDO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Database Access Object for driver table.
 * <p/>
 */
public interface DriverRepository extends JpaRepository<DriverDO, Long>
{
    Optional<DriverDO> findByIdAndDeletedFalse(Long id);
}
