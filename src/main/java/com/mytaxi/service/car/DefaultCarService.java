package com.mytaxi.service.car;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.ManufacturerRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.exception.BusinessLogicException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.exception.NoCarFoundException;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some car specific things.
 * <p/>
 */
@Service
public class DefaultCarService implements CarService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultCarService.class);
    private static final String ENTITY_NOT_FOUND = "Could not find entity with id: ";
    private static final String INVALID_MANUFACTURE = "Invalid manufacture.";

    private final CarRepository carRepository;
    private final ManufacturerRepository manufacturerRepository;

    public DefaultCarService(
        final CarRepository carRepository,
        final ManufacturerRepository manufacturerRepository)
    {
        this.carRepository = carRepository;
        this.manufacturerRepository = manufacturerRepository;
    }

    /**
     * Search for a car by carId
     * @param carId - Car id
     * @return a car based on carId
     * @throws EntityNotFoundException if no car with the given carId is found
     */
    @Override
    public CarDO find(Long carId) throws EntityNotFoundException
    {
        LOG.info("Searching for the car with id {}", carId);
        return findCar(carId);
    }

    /**
     * Search for all cars
     * @return all cars
     * @throws NoCarFoundException when no car is found
     */
    @Override
    public List<CarDO> findAll() throws NoCarFoundException
    {
        LOG.info("Searching for all cars");
        List<CarDO> cars = carRepository.findAllByDeletedFalse();

        if (cars.isEmpty())
        {
            throw new NoCarFoundException("No car was found!");
        }

        return cars;
    }

    /**
     * Creates a new car
     * @param carDO - Car domain object
     * @return the created car
     * @throws ConstraintsViolationException if a car already exists with the given licensePlate
     * @throws BusinessLogicException if something breaks the business logic
     */
    @Override
    public CarDO create(CarDO carDO) throws ConstraintsViolationException, BusinessLogicException
    {
        CarDO car;

        try
        {
            findAndValidateManufacturer(carDO);
            LOG.info("Saving car with license plate {}", carDO.getLicensePlate());
            car = carRepository.save(carDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("Some constraints are thrown due to car creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return car;
    }

    private void findAndValidateManufacturer(CarDO carDO) throws BusinessLogicException
    {
        if (carDO.getManufacturer() != null && carDO.getManufacturer().getId() != null)
        {
            Optional<ManufacturerDO> manufacturerDO = manufacturerRepository.findById(carDO.getManufacturer().getId());

            if (manufacturerDO.isPresent())
            {
                carDO.setManufacturer(manufacturerDO.get());
            } else {
                throw new BusinessLogicException(INVALID_MANUFACTURE);
            }
        }
    }

    /**
     * Deletes an existing car by carId
     * @param carId - Car id
     * @throws EntityNotFoundException if no car with the given carId is found
     */
    @Override
    @Transactional
    public void delete(Long carId) throws EntityNotFoundException
    {
        CarDO carDO = findCar(carId);
        LOG.info("Deleting the car with id {}", carId);
        carDO.setDeleted(true);
    }

    /**
     * Updates a car
     * @param carDO - Car domain object
     * @param carId - Car id
     * @return the updated car
     * @throws EntityNotFoundException if no car with the given carId is found
     * @throws ConstraintsViolationException if a car already exists with the given licensePlate
     * @throws BusinessLogicException if something breaks the business logic
     */
    @Override
    @Transactional
    public CarDO update(CarDO carDO, Long carId) throws EntityNotFoundException, ConstraintsViolationException, BusinessLogicException
    {
        LOG.info("Searching for car with id {} to be updated", carId);
        CarDO car = findCar(carId);
        car.setLicensePlate(carDO.getLicensePlate());
        car.setSeatCount(carDO.getSeatCount());
        car.setConvertible(carDO.getConvertible());
        car.setRating(carDO.getRating());
        car.setEngineType(carDO.getEngineType());
        car.setManufacturer(carDO.getManufacturer());
        findAndValidateManufacturer(car);

        try
        {
            LOG.info("Updating car with license plate {}", car.getLicensePlate());
            car = carRepository.save(car);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("Some constraints are thrown due to car creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return car;
    }

    private CarDO findCar(Long carId) throws EntityNotFoundException
    {
        return carRepository.findByIdAndDeletedFalse(carId)
            .orElseThrow(() -> new EntityNotFoundException(ENTITY_NOT_FOUND + carId));
    }
}
