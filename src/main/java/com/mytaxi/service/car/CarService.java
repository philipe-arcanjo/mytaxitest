package com.mytaxi.service.car;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.BusinessLogicException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.exception.NoCarFoundException;

import java.util.List;

public interface CarService
{
    CarDO find(Long carId) throws EntityNotFoundException;

    List<CarDO> findAll() throws NoCarFoundException;

    CarDO create(CarDO carDO) throws BusinessLogicException, ConstraintsViolationException;

    void delete(Long carId) throws EntityNotFoundException;

    CarDO update(CarDO carDO, Long carId) throws BusinessLogicException, EntityNotFoundException, ConstraintsViolationException;
}
