package com.mytaxi.service.driver;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.BusinessLogicException;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.List;

public interface DriverService
{

    DriverDO find(Long driverId) throws EntityNotFoundException;

    DriverDO create(DriverDO driverDO) throws ConstraintsViolationException;

    void delete(Long driverId) throws EntityNotFoundException;

    void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException;

    List<DriverDO> findByAttributes(OnlineStatus onlineStatus, String userName, String licensePlate, Integer rating, Integer seatCount, Boolean convertible);

    DriverDO selectCar(long driverId, CarDO carDO) throws BusinessLogicException, CarAlreadyInUseException, EntityNotFoundException;

    DriverDO deselectCar(long driverId, CarDO carDO) throws EntityNotFoundException, BusinessLogicException;
}
