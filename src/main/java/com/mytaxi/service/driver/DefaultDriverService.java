package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.BusinessLogicException;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private static final String ENTITY_NOT_FOUND = "Could not find entity with id: ";
    private static final String ENTITY_CAR_NOT_FOUND = "Could not find car with license plate: ";
    private static final String CAR_CANNOT_BE_SELECTED = "A car can be selected only by ONLINE drivers.";
    private static final String CAR_ALREADY_IN_USE = "The car is already in use.";
    private static final String DRIVER_ALREADY_WITH_CAR = "The driver already has a selected car.";
    private static final String SEARCHING_DRIVER_ID = "Searching for the driver with id {}";
    private static final String SEARCHING_CAR_LICENSE_PLATE = "Searching for the car with license plate {}";
    private static final String DRIVER_WITHOUT_GIVEN_LICENSE_PLATE = "The driver does not have a selected car with the given license plate: ";
    private static final String DRIVER_DOES_NOT_HAVE_A_CAR = "The driver does not have any car selected.";

    private final DriverRepository driverRepository;
    private final CarRepository carRepository;

    public DefaultDriverService(
        final DriverRepository driverRepository, final CarRepository carRepository)
    {
        this.driverRepository = driverRepository;
        this.carRepository= carRepository;
    }

    /**
     * Selects a driver by id.
     * @param driverId - Driver id
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException
    {
        LOG.info(SEARCHING_DRIVER_ID, driverId);
        return findDriverChecked(driverId);
    }

    /**
     * Creates a new driver.
     * @param driverDO - Driver domain object
     * @return the created driver
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException
    {
        DriverDO driver;
        try
        {
            LOG.info("Saving driver with username {}", driverDO.getUsername());
            driver = driverRepository.save(driverDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("Some constraints are thrown due to driver creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }

    /**
     * Deletes an existing driver by id.
     * @param driverId -  Driver id
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        LOG.info("Deleting the driver with id {}", driverId);
        driverDO.setDeleted(true);
    }

    /**
     * Update the location for a driver.
     * @param driverId - Driver id
     * @param longitude - Longitude coordinate
     * @param latitude - Latitude coordinate
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        LOG.info("Updating the location of driver with id {}", driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }

    /**
     * Find drivers by attributes (not required) below
     * @param onlineStatus - Status(ONLINE/OFFLINE)
     * @param userName - Username of the driver
     * @param licensePlate - License plate of selected car
     * @param rating - Rating of selected car
     * @param seatCount - Seat count of selected car
     * @param convertible - If the selected car is convertible of not
     * @return the list of drivers
     */
    @Override
    public List<DriverDO> findByAttributes(OnlineStatus onlineStatus, String userName,
        String licensePlate, Integer rating, Integer seatCount, Boolean convertible)
    {
        LOG.info("Searching for drivers...");

        DriverDO driverDO = new DriverDO();
        driverDO.setOnlineStatus(onlineStatus);
        driverDO.setUsername(userName);

        if (licensePlate != null || rating != null || seatCount != null || convertible  != null) {
            CarDO carDO = new CarDO();
            carDO.setLicensePlate(licensePlate);
            carDO.setRating(rating);
            carDO.setSeatCount(seatCount);
            carDO.setConvertible(convertible);

            driverDO.setCar(carDO);
        }

        ExampleMatcher matcher = ExampleMatcher.matching()
            .withIgnorePaths("dateCreated", "dateCoordinateUpdated", "car.busy", "car.dateCreated");

        return driverRepository.findAll(Example.of(driverDO, matcher));
    }

    /**
     * Selects a car based on license plate to the given driver
     * @param driverId - Driver id
     * @param selectCarDO - Select Car domain object
     * @return the driver with the car selected
     * @throws EntityNotFoundException if no driver with the given id was found.
     * @throws BusinessLogicException if something breaks the business logic
     * @throws CarAlreadyInUseException if a car is already in use
     */
    @Override
    @Transactional
    public DriverDO selectCar(long driverId, CarDO selectCarDO) throws EntityNotFoundException, BusinessLogicException, CarAlreadyInUseException
    {
        LOG.info(SEARCHING_DRIVER_ID, driverId);
        DriverDO driverDO = findDriverChecked(driverId);
        validateDriverForSelect(driverDO);

        LOG.info(SEARCHING_CAR_LICENSE_PLATE, selectCarDO.getLicensePlate());
        CarDO carDO = findCar(selectCarDO.getLicensePlate());
        validateCar(carDO);
        carDO.setBusy(true);

        driverDO.setCar(carDO);
        LOG.info("Updating driver {} with selected car with license plate {}", driverId, selectCarDO.getLicensePlate());
        return driverRepository.save(driverDO);
    }

    private void validateDriverForSelect(DriverDO driverDO) throws BusinessLogicException
    {
        if (driverDO.getOnlineStatus().equals(OnlineStatus.OFFLINE))
        {
            throw new BusinessLogicException(CAR_CANNOT_BE_SELECTED);
        }

        if (driverDO.getCar() != null)
        {
            throw new BusinessLogicException(DRIVER_ALREADY_WITH_CAR);
        }
    }

    private void validateCar(CarDO carDO) throws CarAlreadyInUseException
    {
        if (carDO.isBusy())
        {
            throw new CarAlreadyInUseException(CAR_ALREADY_IN_USE);
        }
    }

    /**
     * Deselects a car to the given driver
     * @param driverId - Driver id
     * @param deselectCarDO - Deselect Car domain object
     * @return the driver with no car selected
     * @throws EntityNotFoundException if no driver with the given id was found
     * @throws BusinessLogicException if something breaks the business logic
     */
    @Override
    @Transactional
    public DriverDO deselectCar(long driverId, CarDO deselectCarDO) throws EntityNotFoundException, BusinessLogicException
    {
        LOG.info(SEARCHING_DRIVER_ID, driverId);
        DriverDO driverDO = findDriverChecked(driverId);
        validateDriverForDeselect(driverDO, deselectCarDO.getLicensePlate());
        driverDO.setCar(null);

        LOG.info(SEARCHING_CAR_LICENSE_PLATE, deselectCarDO.getLicensePlate());
        CarDO carDO = findCar(deselectCarDO.getLicensePlate());
        carDO.setBusy(false);

        LOG.info("Updating driver {} to be without a selected car", driverId);
        return driverRepository.save(driverDO);
    }

    private void validateDriverForDeselect(DriverDO driverDO, String licensePlate) throws BusinessLogicException
    {
        if (driverDO.getCar() == null)
        {
            throw new BusinessLogicException(DRIVER_DOES_NOT_HAVE_A_CAR);
        }

        if (!driverDO.getCar().getLicensePlate().equals(licensePlate))
        {
            throw new BusinessLogicException(DRIVER_WITHOUT_GIVEN_LICENSE_PLATE + licensePlate);
        }
    }

    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException
    {
        return driverRepository.findByIdAndDeletedFalse(driverId)
            .orElseThrow(() -> new EntityNotFoundException(ENTITY_NOT_FOUND + driverId));
    }

    private CarDO findCar(String licensePlate) throws EntityNotFoundException
    {
        return carRepository.findByLicensePlateAndDeletedFalse(licensePlate)
            .orElseThrow(() -> new EntityNotFoundException(ENTITY_CAR_NOT_FOUND + licensePlate));
    }

}
