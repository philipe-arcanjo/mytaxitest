package com.mytaxi.datatransferobject;

import javax.validation.constraints.NotNull;

public class SelectCarDTO
{

    @NotNull(message = "License plate can not be null!")
    private String licensePlate;

    private SelectCarDTO()
    {
    }

    public String getLicensePlate()
    {
        return licensePlate;
    }
}
