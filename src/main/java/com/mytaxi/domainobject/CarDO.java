package com.mytaxi.domainobject;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(
    name = "car",
    uniqueConstraints = @UniqueConstraint(name = "license_plate", columnNames = {"licensePlate"})
)
public class CarDO
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false)
    private Boolean deleted = false;

    @Column(nullable = false)
    @NotNull(message = "License plate can not be null!")
    private String licensePlate;

    @Column(nullable = false)
    @NotNull(message = "Seat count can not be null!")
    private Integer seatCount;

    @Column(nullable = false)
    @NotNull(message = "Convertible can not be null!")
    private Boolean convertible;

    @Column
    private Integer rating;

    @Column
    private String engineType;

    @ManyToOne
    private ManufacturerDO manufacturer;

    @Column
    private boolean busy;

    public CarDO()
    {
    }

    public CarDO(String licensePlate, Integer seatCount, Boolean convertible, Integer rating,
                 String engineType, ManufacturerDO manufacturer)
    {
        this.deleted = false;
        this.busy = false;
        this.licensePlate = licensePlate;
        this.seatCount = seatCount;
        this.convertible = convertible;
        this.rating = rating;
        this.engineType = engineType;
        this.manufacturer = manufacturer;
    }

    public CarDO(String licensePlate)
    {
        this.licensePlate = licensePlate;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean getDeleted()
    {
        return deleted;
    }

    public void setDeleted(Boolean deleted)
    {
        this.deleted = deleted;
    }

    public String getLicensePlate()
    {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate)
    {
        this.licensePlate = licensePlate;
    }

    public Integer getSeatCount()
    {
        return seatCount;
    }

    public void setSeatCount(Integer seatCount)
    {
        this.seatCount = seatCount;
    }

    public Boolean getConvertible()
    {
        return convertible;
    }

    public void setConvertible(Boolean convertible)
    {
        this.convertible = convertible;
    }

    public Integer getRating()
    {
        return rating;
    }

    public void setRating(Integer rating)
    {
        this.rating = rating;
    }

    public String getEngineType()
    {
        return engineType;
    }

    public void setEngineType(String engineType)
    {
        this.engineType = engineType;
    }

    public ManufacturerDO getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(ManufacturerDO manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public boolean isBusy()
    {
        return busy;
    }

    public void setBusy(boolean busy)
    {
        this.busy = busy;
    }

    @Override public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        CarDO carDO = (CarDO) o;
        return busy == carDO.busy &&
            Objects.equals(id, carDO.id) &&
            Objects.equals(dateCreated, carDO.dateCreated) &&
            Objects.equals(deleted, carDO.deleted) &&
            Objects.equals(licensePlate, carDO.licensePlate) &&
            Objects.equals(seatCount, carDO.seatCount) &&
            Objects.equals(convertible, carDO.convertible) &&
            Objects.equals(rating, carDO.rating) &&
            Objects.equals(engineType, carDO.engineType) &&
            Objects.equals(manufacturer, carDO.manufacturer);
    }

    @Override public int hashCode()
    {
        return Objects.hash(id, dateCreated, deleted, licensePlate, seatCount, convertible, rating, engineType, manufacturer, busy);
    }
}
